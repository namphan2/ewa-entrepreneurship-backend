package hva.ewa.Entrepreneurship.controller;

import hva.ewa.Entrepreneurship.repository.UserRepository;
import hva.ewa.Entrepreneurship.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    // Get list of all users
    @RequestMapping(method = RequestMethod.GET, value = "/users")
    public ResponseEntity retrieveAllUsers(User user) {

        List<User> usersList = userRepository.listAllUsers(user.getId(), user.getEmail(), user.getFirst_name(), user.getLast_name(), user.getRole(), user.getTeacher(), user.getClass_name());

        return new ResponseEntity<>(usersList, HttpStatus.OK);
    }

    // Get list of users that are in the same class
    @RequestMapping(method = RequestMethod.GET, value = "/users/class/{class_name}")
    public ResponseEntity retrieveAllUsersOfSameClass(@PathVariable("class_name") String class_name) {

        List<User> usersFromSameClassList = userRepository.listAllUsersBySameClass(class_name);

        return new ResponseEntity(usersFromSameClassList, HttpStatus.OK);
    }

    // Get list of teachers and their classes
    @RequestMapping(method = RequestMethod.GET, value = "/users/list/class/list")
    public ResponseEntity retrieveAllTeachersAndClasses() {

        List<User> teachersAndClassesList = userRepository.listAllTeachersAndClasses();

        return new ResponseEntity(teachersAndClassesList, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/users")
    public ResponseEntity<Void> createUser(@RequestBody User user) {

        if (userRepository.doesUserExist(user.getEmail())) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));

        userRepository.save(user);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/users/user/{userid}")
    public ResponseEntity<?> getUser(@PathVariable("userid") Integer id) {
        User user = userRepository.findUserById(id);

        if (user == null) {
            System.out.println("User with ID " + id + " not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PutMapping(value = "/users/user/{userid}")
    public ResponseEntity<User> updateUser(@RequestBody User updateUser, @PathVariable("userid") Integer userid) {

        User user = userRepository.findUserById(userid);

        user.setEmail(updateUser.getEmail());
        user.setFirst_name(updateUser.getFirst_name());
        user.setLast_name(updateUser.getLast_name());
        user.setRole(updateUser.getRole());

        userRepository.save(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/users/user/{userid}/class")
    public ResponseEntity updateClassOfUser(@RequestBody User updateUser, @PathVariable("userid") Integer userid) {

        User user = userRepository.findUserById(userid);

        if (user == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        user.setClass_name(updateUser.getClass_name());
        userRepository.save(user);

        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping(value = "/users/user/{userid}")
    public ResponseEntity<User> deleteUser(@PathVariable("userid") Integer userid) {

        User user = userRepository.findUserById(userid);

        userRepository.delete(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    // Can be removed
    @RequestMapping(method = RequestMethod.GET, value = "/users/emails")
    public List<User> existingEmail(User user) {

        return userRepository.findAllEmail(user.getEmail());
    }

    @RequestMapping(method = RequestMethod.GET, value = "/users/user/id/{email}")
    public ResponseEntity findUserIdBasedOnEmail(@PathVariable("email") String email) {

        Integer user_id = userRepository.findByUserEmail(email).getId();

        return new ResponseEntity<>(user_id, HttpStatus.OK);
    }
}
